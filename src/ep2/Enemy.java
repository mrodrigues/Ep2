package ep2;

import java.util.List;
import java.util.Random;

public class Enemy extends Sprite {
    
    private static final int MAX_SPEED_X = 1;
    private int posx;
    private int posy;
    

    public Enemy(int x, int y, String img) {
        
        super(x,y);
       
        initEnemy(img);
        
    }
    
    private void initEnemy(String img) {
        
        imgEnemy(img); 
        
    }
    
    private void imgEnemy(String img){
        loadImage(img);
    }
    
    public void move(){
        
        if (this.x < 0) {
            setVisible(false);
        } else {
            this.x -= MAX_SPEED_X;
	}

    }
    
    public int locationX(List<Enemy> listEnemy, int level){
        
        Random random  = new Random();
        int pos_x=0;
        int i, qntLevel=0;
        boolean unic=false;
        
        if(level == 1){
            qntLevel = 2000;
        }
        else if(level == 2){
            qntLevel = 4000;
        } 
        else if(level == 3){
            qntLevel = 6000;
        }
        
        do{
            pos_x = 511 + random.nextInt(qntLevel);
      
            for(i=0; i < listEnemy.size(); i++){
                if(listEnemy.get(i).getX() == pos_x){
                    break;                
                }
            }
            unic = true; 
            
        }while(unic != true);
        
        return pos_x;
    }
    
    public int locationY(List<Enemy> listEnemy){
        
        Random random  = new Random();
        int posy=0;
        int i;
        boolean unic=false;
        
        do{
            posy = 10 + random.nextInt(485);
      
            for(i=0; i < listEnemy.size(); i++){
                if(listEnemy.get(i).getX() == posy){
                    break;                
                }
            }
            unic = true; 
            
        }while(unic != true);
        
        return posy;
    }
 	
}