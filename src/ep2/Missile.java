package ep2;


public class Missile extends Sprite {
    
    private static final int MAX_SPEED_X = 3;

    public Missile(int x, int y) {
        
        super(x,y);
       
        initMissile();
        
    }
    
    private void initMissile() {
        
        imgMissile(); 
        
    }
    
    private void imgMissile(){
        loadImage("images/missile.png"); 
    }
    
    public void move(){

        this.x += MAX_SPEED_X;
        if(this.x > Game.getWidth()){
                setVisible(false);
        }

    }

 	
}
