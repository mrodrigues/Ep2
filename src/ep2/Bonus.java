package ep2;


import java.util.List;
import java.util.Random;

public class Bonus extends Sprite {
    
    private static final int MAX_SPEED_X = 1;
    private int posx;
    private int posy;
    

    public Bonus(int x, int y, String img) {
        
        super(x,y);
       
        initBonus(img);
        
    }
    
    private void initBonus(String img) {
        
        imgBonus(img); 
        
    }
    
    private void imgBonus(String img){
        loadImage(img);
    }
    
    public void move(){
        
        if (this.x < 0) {
            setVisible(false);
        } else {
            this.x -= MAX_SPEED_X;
	}

    }
    
    public int locationx(List<Bonus> listBonus, int level){
        
        Random random  = new Random();
        int posx=0;
        int i, qntLevel=0;
        boolean unic=false;
        
        if(level == 1){
            qntLevel = 2000;
        }
        else if(level == 2){
            qntLevel = 4000;
        } 
        else if(level == 3){
            qntLevel = 6000;
        }

        do{
            posx = 510 + random.nextInt(qntLevel);
      
            for(i=0; i < listBonus.size(); i++){
                if(listBonus.get(i).getX() == posx){
                    break;                
                }
            }
            unic = true; 
            
        }while(unic != true);
        
        return posx;
    }
    
    public int locationy(List<Bonus> listBonus){
        
        Random random  = new Random();
        int posy=0;
        int i;
        boolean unic=false;
        
        do{
            posy = 10 + random.nextInt(485);
      
            for(i=0; i < listBonus.size(); i++){
                if(listBonus.get(i).getX() == posy){
                    break;                
                }
            }
            unic = true; 
            
        }while(unic != true);
        
        return posy;
    }
 	
}
