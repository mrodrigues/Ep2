package ep2;

import ep2.Game;
import ep2.Map;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseInput implements MouseListener{

        
    public void mouseClicked(MouseEvent e) {
    }

    
    public void mousePressed(MouseEvent e) 
    {
        int mx = e.getX();
        int my = e.getY();
            boolean b;
        //Play Button
        if(mx >= Game.getWidth() / 2 +40 && mx <= Game.getWidth() / 2 + 140)
        {
            if(my >= 150 && my <= 200)
            {
                Map.State = Map.STATE.GAME;
            }
        }
        //Quit Button
        if(mx >= Game.getWidth() / 2 +40 && mx <= Game.getWidth() / 2 + 140)
        {
            if(my >= 250 && my <= 300)
            {
                //Press Quit Button
                System.exit(0);
            }
        }
        
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }
}

