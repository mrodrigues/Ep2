package ep2;

import ep2.MouseInput;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Map extends JPanel implements ActionListener {

    private final int SPACESHIP_X = 2;
    private final int SPACESHIP_Y = 200;
    private final Timer timer_map;


    private final Image background;
    private Spaceship spaceship;
    
    private List<Enemy> enemy;
    private List<Bonus> bonus;
    private boolean stop=false;
    
    public Map() {
        
        addKeyListener(new KeyListerner());
        addKeyListener(new TecladoAdapter());
        this.addMouseListener(new MouseInput());
        
        setFocusable(true);
        setDoubleBuffered(true);
        ImageIcon image = new ImageIcon("images/space.jpg");
        
        this.background = image.getImage();

        spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
        
        initEnemy();
        initBonus();

        timer_map = new Timer(Game.getDelay(), this);
        timer_map.start();
                        
        
    }
    
    public static enum STATE{
        MENU,
        GAME
    }; 
    public static STATE State = STATE.MENU;

    
    @Override
    public void paintComponent(Graphics g) {
        
        super.paintComponent(g);
       
        if(State == STATE.MENU){
            stop = true;
            menu(g);
            timer_map.stop();
        }
        
        else if(State == STATE.GAME && stop == true){
            timer_map.restart();
            stop = false;
        }
        
        else if(spaceship.getLife() > 0 && spaceship.getWinner() == false){
            g.drawImage(this.background, 0, 0, null);

            draw(g);

            List<Missile> missile = spaceship.getMissile();

            for (int i = 0; i < missile.size(); i++) {
                    Missile m = (Missile) missile.get(i);
                    g.drawImage(m.getImage(), m.getX(), m.getY(), this);
            }

            for (int i = 0; i < enemy.size(); i++) {
                    Enemy in = (Enemy) enemy.get(i);
                    g.drawImage(in.getImage(), in.getX(), in.getY(), this);
            }

            for (int i = 0; i < bonus.size(); i++) {
                    Bonus bo = (Bonus) bonus.get(i);
                    g.drawImage(bo.getImage(), bo.getX(), bo.getY(), this);
            }

            g.setColor(Color.WHITE);
            g.drawString("Life: " + spaceship.getLife(), 5, 15);
            g.drawString("Score: " + spaceship.getScore(), 10, 30);
        }
        else if(spaceship.getWinner() == true){
            dranMissionAccomplished(g);
            timer_map.stop();
        }
        else if(spaceship.getLife() == 0){
            drawGameOver(g);
            timer_map.stop();
        }
        
        Toolkit.getDefaultToolkit().sync();
        
    }
    
    private void draw(Graphics g) {
               
        // Draw spaceship
        g.drawImage(spaceship.getImage(), spaceship.getX(), spaceship.getY(), this);
    }
    
    public void initEnemy() {
        
        int coordX=0, coordY=0;
        
        if(spaceship.getLevel() == 1){
            enemy = new ArrayList<Enemy>();
        
            for (int i = 0; i < 70; i++) {
                if(i == 0){
                    enemy.add(new Enemy(510, 300, "images/enemy1.gif"));
                }   
                else {  
                    coordX = enemy.get(i-1).locationX(enemy, spaceship.getLevel());
                    coordY = enemy.get(i-1).locationY(enemy);
                    enemy.add(new Enemy(coordX, coordY, "images/enemy1.gif"));
                }
                
            }
        }
       
        else if(spaceship.getLevel() == 2){
            for (int i = 0; i < 140; i++) {
                if(i == 0){
                    enemy.add(new Enemy(510, 300, "images/enemy2.gif"));
                }
                else{
                    coordX = enemy.get(i-1).locationX(enemy, spaceship.getLevel());
                    coordY = enemy.get(i-1).locationY(enemy);
                    enemy.add(new Enemy(coordX, coordY, "images/enemy2.gif"));
                }
            }
        }
        
        else if(spaceship.getLevel() == 3){
            for (int i = 0; i < 280; i++) {
                if(i == 0){
                    enemy.add(new Enemy(510, 300, "images/enemy3.gif"));
                }
                else{
                    coordX = enemy.get(i-1).locationX(enemy, spaceship.getLevel());
                    coordY = enemy.get(i-1).locationY(enemy);
                    enemy.add(new Enemy(coordX, coordY, "images/enemy3.gif"));
                }
            }
        }
        
}
    public void initBonus() {
        
        int coordX=0, coordY=0;
        
        
        if(spaceship.getLevel() == 1){
            bonus = new ArrayList<Bonus>();
            
            for (int i = 0; i < 40; i++) {
                if(i == 0){
                    bonus.add(new Bonus(515, 300, "images/moeda.gif"));
                }   
                else {  
                    coordX = bonus.get(i-1).locationx(bonus, spaceship.getLevel());
                    coordY = bonus.get(i-1).locationy(bonus);
                    bonus.add(new Bonus(coordX, coordY, "images/moeda.gif"));
                }

            }
        }
        else if(spaceship.getLevel() == 2){
            for (int i = 0; i < 50; i++) {
                if(i == 0){
                    bonus.add(new Bonus(515, 300, "images/moeda.gif"));
                }   
                else {  
                    coordX = bonus.get(i-1).locationx(bonus, spaceship.getLevel());
                    coordY = bonus.get(i-1).locationy(bonus);
                    bonus.add(new Bonus(coordX, coordY, "images/moeda.gif"));
                }

            }
        }
        else if(spaceship.getLevel() == 3){
            for (int i = 0; i < 60; i++) {
                if(i == 0){
                    bonus.add(new Bonus(515, 300, "images/moeda.gif"));
                }   
                else {  
                    coordX = bonus.get(i-1).locationx(bonus, spaceship.getLevel());
                    coordY = bonus.get(i-1).locationy(bonus);
                    bonus.add(new Bonus(coordX, coordY, "images/moeda.gif"));
                }

            }
        } 
        
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        
            
            if (enemy.size() == 0 && spaceship.getLevel() < 3) {
		spaceship.setLevel(spaceship.getLevel()+1);
                initEnemy();
                initBonus();
            }
            
            List<Missile> missile = spaceship.getMissile();

            for (int i = 0; i < missile.size(); i++) {

                    Missile m = (Missile) missile.get(i);

                    if (m.isVisible()) {
                            m.move();
                    } else {
                            missile.remove(i);
                    }

            }
            
            for (int i = 0; i < enemy.size(); i++) {

                    Enemy in = (Enemy) enemy.get(i);

                    if (in.isVisible()) {
                            in.move();
                    } else {
                            enemy.remove(i);
                    }
                    
                    if(spaceship.getLevel() == 3 && enemy.size() == 0){
                        spaceship.setWinner(true);
                    }
            }
            
            for (int i = 0; i < bonus.size(); i++) {

                    Bonus bo = (Bonus) bonus.get(i);

                    if (bo.isVisible()) {
                            bo.move();
                    } else {
                            bonus.remove(i);
                    }

            }
 
        
		
        updateSpaceship();
        isColision();
        repaint();
    }
    
    	public void isColision() {

		Rectangle formSpaceship = spaceship.getBounds();
		Rectangle formEnemy;
		Rectangle formMissile;
                Rectangle formBonus;
                
                List<Missile> missile = spaceship.getMissile();
                
                for (int i = 0; i < missile.size(); i++) {

			Missile tempMissile = missile.get(i);
			formMissile = tempMissile.getBounds();
                
                        for (int j = 0; j < bonus.size(); j++) {

				Bonus tempBonus = bonus.get(j);
				formBonus = tempBonus.getBounds();

				if (formMissile.intersects(formBonus)) {
					tempBonus.setVisible(false);
					tempMissile.setVisible(false);
                                        spaceship.setScore(spaceship.getScore()+5);
				}
			}
                
                }

		for (int i = 0; i < enemy.size(); i++) {

			Enemy tempEnemy = enemy.get(i);
			formEnemy = tempEnemy.getBounds();

			if (formSpaceship.intersects(formEnemy)) {

				spaceship.setVisible(false);
				tempEnemy.setVisible(false);
                                spaceship.setLife(spaceship.getLife()-1);

			}

		}
                
           	for (int i = 0; i < bonus.size(); i++) {

			Bonus tempBonus = bonus.get(i);
			formBonus = tempBonus.getBounds();

			if (formSpaceship.intersects(formBonus)) {

				spaceship.setVisible(false);
				tempBonus.setVisible(false);
                                spaceship.setScore(spaceship.getScore()+10);
			}

		}
		
		for (int i = 0; i < missile.size(); i++) {

			Missile tempMissile = missile.get(i);
			formMissile = tempMissile.getBounds();

			for (int j = 0; j < enemy.size(); j++) {

				Enemy tempEnemy = enemy.get(j);
				formEnemy = tempEnemy.getBounds();

				if (formMissile.intersects(formEnemy)) {

					tempEnemy.setVisible(false);
					tempMissile.setVisible(false);
				}
			}
      			
		}
                
                

	}
        
    private void dranMissionAccomplished(Graphics g) {
        
        g.drawImage(this.background, 0, 0, null);
        String message = "MISSION ACCOMPLISHED";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void drawGameOver(Graphics g) {
        
        g.drawImage(this.background, 0, 0, null);
        String message = "Game Over";
        Font font = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metric = getFontMetrics(font);

        g.setColor(Color.white);
        g.setFont(font);
        g.drawString(message, (Game.getWidth() - metric.stringWidth(message)) / 2, Game.getHeight() / 2);
    }
    
    private void menu(Graphics g) {
        
        Rectangle playButton = new Rectangle(Game.getWidth() / 2 +40, 150, 100, 50);
        Rectangle quitButton = new Rectangle(Game.getWidth() / 2 +40, 250, 100, 50);
        Image image;
        Graphics2D g2d = (Graphics2D) g;
       
        g.drawImage(this.background, 0, 0, null);
        ImageIcon ii = new ImageIcon("images/sonic_fundo.gif");
        image = ii.getImage();
        
        g.drawImage(image, 7, 100, this);
        
        Font fnt0 = new Font("arial", Font.BOLD, 50);
        g.setFont(fnt0);
        g.setColor(Color.white);
        g.drawString("Sonic Space Game", 20, 80);
          
        Font fnt2 = new Font("arial", Font.ITALIC, 15);
        g.setFont(fnt2);
        g.setColor(Color.white);

        Font fnt1 = new Font("arial", Font.BOLD, 30);
        g.setFont(fnt1);
        
        g.drawString("Play", playButton.x + 19, playButton.y + 35);
        g2d.draw(playButton);
        g.drawString("Quit", quitButton.x + 19, quitButton.y + 35);
        g2d.draw(quitButton);
    
    }
    
    private void updateSpaceship() {
        spaceship.move();
    }
  

    private class KeyListerner extends KeyAdapter {
        
        @Override
        public void keyPressed(KeyEvent e) {
            spaceship.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            spaceship.keyReleased(e);
        }
        
    }
    
    	private class TecladoAdapter extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				
                                Map.State = Map.STATE.MENU;
                                timer_map.restart();
                                spaceship = new Spaceship(SPACESHIP_X, SPACESHIP_Y);
				initEnemy();
                                initBonus();
                                
			}
			
			spaceship.keyPressed(e);
		}

		@Override
		public void keyReleased(KeyEvent e) {
			spaceship.keyReleased(e);
		}

	}
        
}