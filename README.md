# EP2 - OO (UnB - Gama)

Este projeto consiste em um jogo desenvolvido em java, onde há interações entre o jogador e os personagens do jogo.
Com tematica baseado nos personagens do famoso jogo sonic.

## Instruções de desenvolvimento:

1. O programa foi criado ultilizando a IDE Netbeans 8.2, pelo sistema operacional windows;

### Instruções de jogabilidade:

1. O jogo possui 3 niveis de dificuldade.
2. O player é representado pelo personagem sonic e seu amigo tails, pilotando um avião.
3. O bonus é representado pelas moedas e acrescenta em 10 pontos no score.
4. O player possui 3 vidas, que são desacrescentado cada ver que o avião encosta em um inimigo, se a vida
chegar á 0, ocorre o "GAME OVER".
5. Caso o player chegue até o final do jogo sem perder todas as vidas, ele vence o jogo.


##### Dados do aluno

 - Nome: Matheus Rodrigues do Nascimento
 - Matricula: 16/0015294
 - Repositorio: https://gitlab.com/mrodrigues/EP2
